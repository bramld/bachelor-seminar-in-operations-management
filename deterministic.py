import numpy as np
import matplotlib.pyplot as plt
import math

# Define existing facility and demand points
existing_facility = {'coordinates': (2, 3), 'quality': [8, 5, 6, 7, 4]}
demand_points = [(4, 4), (2.5, 2), (2.5, 4.5), (2.5, 3), (1.5, 2), (0.5, 1), (0.5, 2.5), (0.5, 3.5)]

# Define quality weights
quality_weights = [0.8, 0.73, 0.72, 0.68, 0.66]

# Define the grid search range
x_min, x_max = 1, 5
y_min, y_max = 1, 5
grid_step = 0.5

# Define the number of simulations for probabilistic market share
num_simulations = 10000

# Define the function to calculate the utility of a facility at a certain location
def calculate_utility(facility, demand_point, weights):
    distance = np.linalg.norm(np.array(facility['coordinates']) - np.array(demand_point))
    utility = (np.dot(weights, facility['quality']) / 1.5 ) - distance
    return utility

# Gets Z Value from Standard Normal Probabilities Table
def z_to_probability(z):
    return 0.5 * (1 + math.erf(z / np.sqrt(2)))

def z_to_two_tailed_probability(z):
    return 2 * (1 - z_to_probability(abs(z)))

# Define the function to calculate the expected market share for a new facility with variance
def expected_market_share_with_variance(new_facility, existing_facility, demand_points, weights, num_simulations):
    total_market_share = 0
    
    for _ in range(num_simulations):
        market_share_New = 0
        for dp in demand_points:
            new_utility = calculate_utility(new_facility, dp, weights)
            existing_utility = calculate_utility(existing_facility, dp, weights)

            zValue = (new_utility - existing_utility) / math.sqrt(1.4 + 1.4)
            two_tailed_probability = z_to_two_tailed_probability(zValue)
            
            
            market_share_new_facility = 1*two_tailed_probability

            market_share_New += market_share_new_facility
        
        total_market_share += market_share_New
    
    return total_market_share / num_simulations

# Perform grid search to find the best location for the new facility
best_location = None
best_market_share = -1

for x in np.arange(x_min, x_max + grid_step, grid_step):
    for y in np.arange(y_min, y_max + grid_step, grid_step):
        new_facility = {'coordinates': (x, y), 'quality': [5, 7, 4, 9, 3]}  
        market_share = expected_market_share_with_variance(new_facility, existing_facility, demand_points, quality_weights, num_simulations)
        if market_share > best_market_share:
            best_market_share = market_share
            best_location = (x, y)

print(f"Best location for the new facility: {best_location} with expected market share: {best_market_share:.2f}")

# Plot the results
#plt.figure(figsize=(10, 8))

img = plt.imread("passau.png")
fig, ax = plt.subplots()
ax.imshow(img, extent=[0, 5, 0, 5])
plt.xticks([0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5])
plt.yticks([0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5])

# Plot existing facility
plt.scatter(*existing_facility['coordinates'], color='red', marker='o', s=100, label='Existing Facility')

# Plot demand points
for dp in demand_points:
    plt.scatter(*dp, color='blue', marker='x', s=100)
    plt.text(dp[0] + 0.1, dp[1] + 0.1, f"({dp[0]},{dp[1]})", fontsize=12)

# Plot the best location for the new facility
plt.scatter(*best_location, color='green', marker='^', s=100, label='New Facility (Best Location)')

# Add grid lines
plt.grid(True)

# Set plot limits
plt.xlim(x_min - 1, x_max + 1)
plt.ylim(y_min - 1, y_max + 1)

# Add labels and title
plt.xlabel('X Coordinate')
plt.ylabel('Y Coordinate')
plt.title('Grid Search for New Facility Location with Probabilistic Market Share and Variance')
plt.legend()
plt.show()
